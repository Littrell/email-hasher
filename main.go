package main

import (
	"crypto/md5"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"time"
)

func main () {
	start := time.Now()

	// Remove hashed emails from a previous run
	os.Remove("emails_hashed.csv")

	file, err := os.Open("emails.csv")
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	hashedFile, err := os.Create("emails_hashed.csv")
	if err != nil {
		log.Fatal(err)
	}
	defer hashedFile.Close()

	csvWriter := csv.NewWriter(hashedFile) 
	defer csvWriter.Flush()

	rows := csv.NewReader(file)
	for {
		row, err := rows.Read()
		if err == io.EOF {
			break
		}

		emailAsBytes := []byte(row[0])

		// Hash
		md5Email := md5.Sum(emailAsBytes)

		// Convert [16]byte to string
		md5String := []string{fmt.Sprintf("%x", md5Email)}

		err = csvWriter.Write(md5String)
		if err != nil {
			log.Fatal(err)
		}
	}

	elapsed := time.Since(start)

	log.Println("emails_hashed.csv written successfully!")
	log.Printf("Operation took %s", elapsed)
}