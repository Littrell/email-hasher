# email-hasher

I wrote this to help me solve a problem at work. It's quick and dirty.

## Build

```bash
go build -o email-hasher *.go
```

## Run

NOTE `email-hasher` requires an `emails.csv` file in the same directory as the binary. It will output a file `emails_hashed.csv`.

```bash
./email-hasher
```
